<?php
/**
 *
 * Description of AdminController
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 22/06/16 23:54
 *
*/
namespace Framework\Lib\Modules\Admin\Controllers;

use \Framework\Core\Controller;

class AdminController extends Controller
{
    public function defaultAction()
    {
        // TODO: Implement defaultAction() method.
    }

    // example
    public function getLoginFormAction()
    {
        // use Users module
    }

    public function postLoginFormAction()
    {
        // v1 example
        $this->getForeignModel('Users')->loginUser(
            $this->getRequest->getParamsPost()
        );
    }
}