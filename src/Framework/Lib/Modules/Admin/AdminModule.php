<?php
/**
 *
 * Description of AdminModule
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/06/16 12:40
 *
*/
namespace Framework\Lib\Modules\Admin;

use \Framework\Core;

use \Framework\Lib;


class AdminModule extends Core\Module
{
    public function __construct()
    {
        parent::__construct(__NAMESPACE__);

        Lib\Logger::debug(__METHOD__ . ' ' . 'Admin Module loaded: ');
    }

}
