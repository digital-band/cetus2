<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 13/06/16
 * Time: 20:06
 */

namespace Framework\Lib\Modules\Users\Controllers;

use \Framework\Core;

class UsersController extends Core\Controller
{

    public function defaultAction()
    {
        // TODO: Implement defaultAction() method.
    }

    public function getLoginFormAction()
    {
        // public request
    }

    public function postLoginFormAction()
    {

    }

    public function getUserPermissions()
    {
        $permissions = $this->getModule()->getActionList(100500);

        \Framework\Lib\Logger::debug(__METHOD__ . ': ' . print_r($permissions, true));
    }
}