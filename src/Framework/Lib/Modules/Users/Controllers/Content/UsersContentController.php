<?php
/**
 *
 * Description of UsersContentController.php
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 13/06/16 19:05
 *
*/

namespace Framework\Lib\Modules\Users\Controllers\Content;

use Framework\Core\Controller;


class UsersContentController extends Controller
{
    public function defaultAction()
    {
        // TODO: Implement defaultAction() method.
    }

    public function getLoginFormAction()
    {
        echo "<input type='text' placeholder='Login'><input type='password' placeholder='Password'>";
    }

    public function postLoginFormAction()
    {
        echo '<br>Login Form posted';
    }
}