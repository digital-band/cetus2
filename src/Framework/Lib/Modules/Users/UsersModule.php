<?php
/**
 *
 * Description of UsersModule.php
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 02/06/16 11:20
 *
*/

namespace Framework\Lib\Modules\Users;


use \Framework\Core;

use \Framework\Lib;

class UsersModule extends Core\Module
{
    public function __construct()
    {
        parent::__construct(__NAMESPACE__);

        $this->registerModel('Users');
        $this->registerModel('Login');
        $this->registerModel('Permissions');
    }

    /**
     * @param int $userId
     * @return array
     * @throws \Framework\Exceptions\WrongParamsException
     */
    protected function getActionList(int $userId)
    {
        $user = $this->getModel('Users')->get($userId);

        return $this->getModel('Permissions')->getList($user);
    }

    /**
     * @param string $actionAlias
     * @param int $userId
     * @return bool
     * @throws \Framework\Exceptions\WrongParamsException
     */
    public function checkUserPermission(string $actionAlias, int $userId)
    {
        // stub
        $user = $this->getModel('Users')->get($userId);

        return $this->getModel('Permissions')->check($user, $actionAlias);
    }
}