<?php
/**
 *
 * Description of User
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/07/16 19:29
 *
*/

namespace Framework\Lib\Modules\Users\Objects;


class User
{
    public function __construct(string $login)
    {
        $this->setLogin($login);
    }

    protected $login;

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin(string $value)
    {
        // TODO validator
        $this->login = $value;
    }

    protected $email;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($value)
    {
        $this->email = $value;
    }

    protected $passwordHash;

    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    public function setPasswordHash($value)
    {
        $this->passwordHash = $value;
    }
}