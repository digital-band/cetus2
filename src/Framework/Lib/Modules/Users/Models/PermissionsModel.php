<?php
/**
 *
 * Description of PermissionsModel
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/07/16 19:03
 *
*/

namespace Framework\Lib\Modules\Users\Models;

use \Framework\Core\Model;
use \Framework\Lib\Modules\Users;

class PermissionsModel extends Model
{
    protected function getList(Users\Objects\User $user)
    {
        return [
            'comment',
            'deleteComment',
            'editComment'
        ];
    }

    public function check(Users\Objects\User $user, string $actionAlias)
    {
        // stub
        // TODO
        error_log(__METHOD__);

        return true;
    }
}