<?php
/**
 *
 * Description of UsersModel
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 02/06/16 12:06
 *
*/

namespace Framework\Lib\Modules\Users\Models;

use \Framework\Core;
use \Framework\Lib\Modules\Users\Objects;

class UsersModel extends Core\Model
{
    public function init($module)
    {
        parent::__construct($module);

        echo 'Lib module Users loaded. UsersModel', '<br>';
    }

    public function get(int $userId)
    {
        // TODO
        // stub

        return new Objects\User('TestUser1');
    }
}