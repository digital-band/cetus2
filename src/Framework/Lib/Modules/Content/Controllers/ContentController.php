<?php
/**
 *
 * Description of Content
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 22/05/16 18:52
 *
*/

namespace Framework\Lib\Modules\Content\Controllers;

use \Framework\Core;
use \Framework\Lib;
use \Framework\Lib\Modules\Content\Objects;


class ContentController extends Core\Controller
{
    // action<action_method><resource>

    /*
     *
     *  - Get content page              GET     //content/pages/100500
        - Get content page              GET     //some/page/string/uri
        - Get content page              GET     //content/pagecategory/pagename
        - Update content page           POST    //content/pagecategory/pagename/update
        - Add page in category (show)   GET     //content/categories/45/pages/add
        - Add page in category (post)   POST    //content/categories/45/pages/add

    <module:\w+>/<resource:\w>/

    Method: GET
    URI:    /categories/{categoryId}/pages/add
    Action: addPageGet

    Method: POST
    URI:    /categories/{categoryId}/pages/add
    Action: addPagePost

     *
     *
     * */

    public function default()
    {
        echo 'Action Default', PHP_EOL;
        echo $this->getModel(), PHP_EOL;
    }

    public function actionGetPage()
    {
        echo 'Action Get', PHP_EOL;
        //echo print_r($this->getModel(), true);

    }

    public function getUpdatePage(string $uri)
    {

        echo 'Show form: Action PageUpdate', PHP_EOL;
    }

    public function postUpdatePage(array $entry)
    {

        echo 'Send form: Action PageUpdate', PHP_EOL;
    }

    /*public function actionAddPage(array $entry)
    {
        echo 'Action PageAdd', PHP_EOL;
    }*/

    // example
    public function getLoginForm()
    {
        // use Content or Users tpl
    }

    public function postLoginForm()
    {
        // v1 example
        $this->getForeignModel('Users')->loginUser(
            $this->getRequest->getParamsPost()
        );
    }


    public function getCategoriesAdd()
    {
        echo 'Show form: getCategoriesAddAction ', '<br>';
    }

    public function postCategoriesAdd()
    {
        echo 'Send form: postCategoriesAddAction ', '<br>';
    }

    public function actionCategoryAdd()
    {
        echo 'Action CategoryAdd ', '<br>';
    }

    public function getComment($id, $commentId)
    {
        echo 'GetComment Action ', '<br>';

        echo 'GetComment Action $id: ', $id, '<br>';
        echo 'GetComment Action $commentId: ', $commentId, '<br>';
    }


    // auto: <method><Action><Collection>Action

    // categories/{id}/pages/{category_id}/comments
    public function getAddCommentForm($topic, $comment)
    {
        echo 'Add comment GET custom routing';
    }

    public function getUpdateComments($topics, $comments)
    {
        echo 'Update comment GET auto routing';
    }

    public function addComment($topic, $comment)
    {
        echo 'Add comment POST with routing';

        // here need to check user permissions
        // request Users module
        // run checkPermission method
        //$this->getForeignModel();

        $isPermitted = false;   //checkPermission

        if ($isPermitted === true) {
            $comment = new Objects\Comment();
            $comment->setText('A long time ago in a galaxy far far away..');
            $comment->getUserName('Chewbacca');

            $this->getModel('Comment')->add($comment);
        }
    }

    public function getAddComments($topics, $comments)
    {
        echo 'Add comment: GET action auto detect';
    }

    public function postAddComments($topics, $comments)
    {
        echo 'Add comment: POST action auto detect';
    }


    public function postCommentsAdd(){

    }

    // /content/categories/45/pages/add

    public function putUserComment($slug, $id)
    {} // "put_user_comment"     [PUT] /users/{slug}/comments/{id}

    public function postUserCommentVote($slug, $id)
    {} // "post_user_comment_vote" [POST] /users/{slug}/comments/{id}/votes

    public function removeUserComment($slug, $id)
    {}

}