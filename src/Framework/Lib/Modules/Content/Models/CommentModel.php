<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06/07/16
 * Time: 21:50
 */

namespace Framework\Lib\Modules\Content\Models;

use \Framework\Core\Model;
use Framework\Lib\Modules\Content\Objects;

class CommentModel extends Model
{
    public function get(int $id)
    {

    }

    public function add(Objects\Comment $comment)
    {
        // TODO
        // stub

        \Framework\Lib\Logger::debug(__METHOD__ . 'Comment added: ' . print_r($comment, true));

        return true;
    }

    public function update(Objects\Comment $comment)
    {

    }
}