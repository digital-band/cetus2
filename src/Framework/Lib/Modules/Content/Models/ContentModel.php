<?php
/**
 *
 * Description of Content
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 22/05/16 18:00
 *
*/

namespace Framework\Lib\Modules\Content\Models;

use \Framework\Core;

use \Framework\Lib;

class ContentModel extends Core\Model
{
    public function __construct($module)
    {
        parent::__construct($module);

        echo 'Lib module Content loaded. ContentModel', '<br>';

        $feedbackModel = $this->getModule()->getModel('Feedback');

        $feedbackModel->test();

        Lib\Logger::debug(__METHOD__ . ' ' . 'ContentModel __construct Done: ');
    }
}