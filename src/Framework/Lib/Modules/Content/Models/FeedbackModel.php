<?php
/**
 *
 * Description of Feedback
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 22/05/16 18:51
 *
*/
namespace Framework\Lib\Modules\Content\Models;

use \Framework\Core;

use \Framework\Lib;

class FeedbackModel extends Core\Model
{
    public function __construct($module)
    {
        parent::__construct($module);

        echo 'Lib module Content loaded. FeedbackModel', '<br>';

        Lib\Logger::debug(__METHOD__ . ' ' . 'FeedbackModel __construct Done: ');
    }

    public function test()
    {
        Lib\Logger::debug(__METHOD__ . ' ' . ': ' . print_r('Loaded from another model', true));
    }

}