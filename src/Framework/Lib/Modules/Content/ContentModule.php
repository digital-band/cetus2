<?php
/**
 *
 * Description of ContentModule
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 01/06/16 19:44
 *
*/

namespace Framework\Lib\Modules\Content;

use \Framework\Core;

use \Framework\Lib;

class ContentModule extends Core\Module
{
    public function __construct()
    {
        parent::__construct(__NAMESPACE__);

        Lib\Logger::debug(__METHOD__ . ' ' . ': ' . print_r(__NAMESPACE__, true));

        $this->registerModel('Feedback');
        $this->registerModel('Content');

        $this->registerForeignModel('Users', 'Login');
        $this->registerForeignModel('Users', 'Users');



        Lib\Logger::debug(__METHOD__ . ' ' . 'ContentModule __construct Done: ');
    }
}