<?php
/**
 *
 * Description of Comment
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 06/07/16 21:56
 *
*/

namespace Framework\Lib\Modules\Content\Objects;


class Comment
{
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId($value)
    {
        $this->id = $value;
    }

    protected $userName;
    
    public function getUserName()
    {
        return $this->userName;
    }
    
    public function setUserName($value)
    {
        $this->userName = $value;
    }
    
    protected $text;
    
    public function getText()
    {
        return $this->text;
    }
    
    public function setText($value)
    {
        $this->text = $value;
    }
    
    protected $timestamp;
    
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    
    public function setTimestamp($value)
    {
        $this->timestamp = $value;
    }
    
    protected $pageURI;
    
    public function getPageURI()
    {
        return $this->pageURI;
    }
    
    public function setPageURI($value)
    {
        $this->pageURI = $value;
    }
}