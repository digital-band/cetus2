<?php
/**
 *
 * Description of Logger
 *
 * Implements basic methods for logging
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 05/05/16 22:59
 *
*/
namespace Framework\Lib;


/**
 * Class Logger
 * @package Framework\Lib
 */
class Logger
{
    const LOG_TYPE_DEBUG = 'DEBUG';

    const LOG_TYPE_ERROR = 'ERROR';

    const LOG_TYPE_WARNING = 'WARNING';

    const LOG_TYPE_NOTICE = 'NOTICE';

    const LOG_TYPE_INFO = 'INFO';

    /**
     *
     * Writes Warning into log file
     * 
     * @param String $text
     * 
     * @return Int
     *
    */
    public static function warning($text)
    {
        $logger = new self();

        $logger->writeToFile(LOG_PATH . '/common', self::LOG_TYPE_WARNING, $text);
    }

    /**
     *
     * Writes error message into log file
     *
     * @param String $text
     * @param Int $code
     *
     * @return Int
     *
     */
    public static function error($text, $code = null)
    {
        $logger = new self();

        $logger->writeToFile(LOG_PATH . '/error', self::LOG_TYPE_ERROR, $code . ' | ' . $text);
    }
    
    /**
     *
     * Writes debug info into log file
     *
     * @param String $text
     *
     * @return Int
     *
     */
    public static function debug($text, $force = false)
    {
        $logger = new self();

        if (ENV_TYPE === 'DEV' || $force === true) {
            $logger->writeToFile(LOG_PATH . '/debug', self::LOG_TYPE_DEBUG, $text);
        }
    }

    /**
     * @param string $path
     * @param string $type
     * @param string $text
     *
     */
    protected function writeToFile($path, $type = '', $text)
    {
        // TODO detect app
        // TODO push lines to stack

        try {
            file_put_contents(
                $path
                . '.log', '[' . date($this->getDateTimeFormat(), time()) . '] '
                . $type
                . ':  '
                . $text
                . "\r", FILE_APPEND | LOCK_EX
            );
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    protected $dateTimeFormat = 'd-M-y H:i:s T O';

    /**
     * @return string
     */
    public function getDateTimeFormat()
    {
        return $this->dateTimeFormat;
    }

    /**
     * @param string $value
     */
    public function setDateTimeFormat(string $value)
    {
        $this->dateTimeFormat = $value;
    }

    protected $logPath = LOG_PATH;

    /**
     * @return string
     */
    public function getLogPath()
    {
        return $this->logPath;
    }

    /**
     * @param string $value
     */
    public function setLogPath(string $value)
    {
        $this->logPath = $value;
    }

}