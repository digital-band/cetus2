<?php

/**
 *
 * Description of Utils
 *
 * Static methods for project wide use
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/05/16 16:42
 *
*/

namespace Framework\Lib;


/**
 * Class Utils
 * @package Framework\Lib
 */

use Framework\Lib;

class Utils
{
    /**
     * Run function $count times and logs execution time
     *
     * @param $function
     * @param int $count
     * @return mixed
     */
    public static function testTime($function, $count = 1)
    {
        $start_time = microtime(true);

        for ($i = 0; $i < $count; $i++) {
            $result = call_user_func($function);
        }

        $end_time = microtime(true);
        $duration = $end_time - $start_time;
        Lib\Logger::debug(__METHOD__ . ': ' . print_r('Execution time: ' . $duration, true), true);

        return $result;
    }

    /**
     * Recursively merges arrays
     * php function array_merge has different behavior
     *
     * @param array $targetArray
     * @param array $newArray
     * @return array
     */
    public static function mergeArrays(array $targetArray, array $newArray) : array
    {
        foreach ($newArray as $propName => $propValue) {

            if (is_array($propValue) === true
                && array_key_exists($propName, $targetArray) === true
                && is_array($targetArray[$propName]) === true)
            {
                if ( array_key_exists($propName, $targetArray) ) {
                    $targetArray[$propName] = self::mergeArrays($targetArray[$propName], $propValue);
                } else {
                    $targetArray[$propName] = $newArray;
                }

            } else {
                $targetArray[$propName] = $propValue;
            }
        }

        return $targetArray;
    }

    /**
     * Switch key in associative array to value from $newKey property
     *
     * @param array $array
     * @param string $newKey
     * @param string $oldKey    to save previous key
     * @return array
     */
    public static function switchArrayKey(array $array, string $newKey, string $oldKey = null) : array
    {
        $result = [];

        foreach ($array as $key => $item) {
            if ( array_key_exists($newKey, $item) === true && $item[$newKey] !== '' ) {
                // save old key to alias field
                if ( $oldKey !== null ) {
                    $item[$oldKey] = $key;
                }
                $result[$item[$newKey]] = $item;
            }
        }

        return $result;
    }


    public static function pathToNamespace(string $path, string $root = null)
    {
        Lib\Logger::debug(__METHOD__ . ' IN' . ': ' . print_r($path, true));
        $resultPath = '';

        if ( is_dir($path) ) {
            $resultPath = realpath($path);

            Lib\Logger::debug(__METHOD__ . ' $realPath' . ': ' . print_r($resultPath, true));
            //Lib\Logger::debug(__METHOD__ . ' $dir' . ': ' . print_r($dir, true));

        } else {
            $fullPath = $path;
            if ( preg_match('#\.php#', $path ) === 0 ) {
                $fullPath .= '.php';
            }

            $realPath = realpath($fullPath);

            $parts = explode('.', $realPath);

            Lib\Logger::debug(__METHOD__ . ' $realPath' . ': ' . print_r($realPath, true));
            //Lib\Logger::debug(__METHOD__ . ' $parts' . ': ' . print_r($parts, true));

            if (array_key_exists('1', $parts) === true && $parts[1] === 'php') {
                $classPath = $parts[0];
            } else {
                return false;
            }


            //Lib\Logger::debug(__METHOD__ . ' $classPath' . ': ' . print_r($classPath, true));

            if ( file_exists($realPath) === true ) {
                $resultPath = $classPath;
            }
        }

        if ( $root !== null ) {
            $resultPath = str_replace(realpath($root), '', $resultPath);

        }

        $result = str_replace('/', '\\', $resultPath);
        Lib\Logger::debug(__METHOD__ . ' OUT' . ': ' . print_r($result, true));

        return $result;
    }
}