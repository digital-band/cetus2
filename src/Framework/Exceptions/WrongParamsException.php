<?php
/**
 *
 * Description of WrongParamsException
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 16/05/16 21:17
 *
*/
namespace Framework\Exceptions;


/**
 * Class WrongParamsException
 * @package Framework\Exceptions
 */
class WrongParamsException extends \Exception
{
    protected $errorCode;

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param int $value
     */
    public function setErrorCode(int $value)
    {
        $this->errorCode = $value;
    }
    
    protected $errorText;

    /**
     * @return string
     */
    public function getErrorText()
    {
        return $this->errorText;
    }

    /**
     * @param int $value
     */
    public function setErrorText(int $value)
    {
        $this->errorText = $value;
    }
}