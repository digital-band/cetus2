<?php
/**
 *
 * Description of Request
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 18/05/16 20:43
 *
*/
namespace Framework\Constants;


/**
 * Class Request
 * @package Framework\Constants
 */
class Request
{
    const METHOD_POST = 'POST';

    const METHOD_GET = 'GET';

    const METHOD_PUT = 'PUT';

    const METHOD_PATCH = 'PATCH';

    const METHOD_DELETE = 'DELETE';
}