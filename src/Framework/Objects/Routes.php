<?php
/**
 *
 * Description of Routes.php
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 30/05/16 17:35
 *
*/

namespace Framework\Objects;


class Routes
{
    protected $routes;

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param array $value
     */
    public function setRoutes(array $value)
    {
        $this->routes = $value;
    }
}