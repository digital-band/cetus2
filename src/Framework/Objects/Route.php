<?php
/**
 *
 * Description of Route.php
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 31/05/16 13:13
 *
*/

namespace Framework\Objects;


class Route
{
    protected $controllerName;

    public function getControllerName() : string
    {
        return $this->controllerName;
    }

    public function setControllerName(string $value)
    {
        $this->controllerName = $value;
    }

    protected $actionMethod;

    public function getActionMethod() : string
    {
        return $this->actionMethod;
    }

    public function setActionMethod(string $value)
    {
        $this->actionMethod = $value;
    }

    protected $arguments;

    public function getArguments() : array
    {
        return $this->arguments;
    }

    public function setArguments(array $value)
    {
        $this->arguments = $value;
    }
}