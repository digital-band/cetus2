<?php
/**
 *
 * Description of Resource
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 14/05/16 16:38
 *
*/

namespace Framework\Objects;


/**
 * Class Resource
 * @package Framework\Objects
 */
class Resource
{
    protected $URI = '';

    /**
     * @return string
     */
    public function getURI() : string
    {
        return $this->URI;
    }

    /**
     * @param string $value
     */
    public function setURI(string $value)
    {
        $this->URI = $value;
    }
    
    protected $uriParts = [];

    /**
     * @return array
     */
    public function getUriParts()
    {
        return $this->uriParts;
    }

    /**
     * @param array $value
     */
    public function setUriParts(array $value)
    {
        $this->uriParts = $value;
    }

    protected $collections = [];

    /**
     * @return array
     */
    public function getCollections() : array
    {
        return $this->collections;
    }

    /**
     * @param array $value
     */
    public function setCollections(array $value)
    {
        $this->collections = $value;
    }

    protected $collectionMembers = [];

    /**
     * @return array
     */
    public function getCollectionMembers() : array
    {
        return $this->collectionMembers;
    }

    /**
     * @param array $value
     */
    public function setCollectionMembers(array $value)
    {
        $this->collectionMembers = $value;
    }
}