<?php
/**
 *
 * Description of Request
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 15/05/16 16:42
 *
*/

namespace Framework\Objects;


/**
 * Class Request
 * @package Framework\Objects
 */
class Request
{
    protected $URI;
    
    public function getURI()
    {
        return $this->URI;
    }

    /**
     * @param string $value
     */
    public function setURI(string $value)
    {
        $this->URI = $value;
    }
    
    protected $method;
    
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $value
     */
    public function setMethod(string $value)
    {
        $this->method = $value;
    }
    
    protected $paramsGET;
    
    public function getParamsGET()
    {
        return $this->paramsGET;
    }

    /**
     * @param array $value
     */
    public function setParamsGET(array $value)
    {
        $this->paramsGET = $value;
    }
    
    protected $paramsPOST;
    
    public function getParamsPOST()
    {
        return $this->paramsPOST;
    }

    /**
     * @param array $value
     */
    public function setParamsPOST(array $value)
    {
        $this->paramsPOST = $value;
    }
    
    protected $params;
    
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $value
     */
    public function setParams(array $value)
    {
        $this->params = $value;
    }
    
    protected $paramsPUT;
    
    public function getParamsPUT()
    {
        return $this->paramsPUT;
    }

    /**
     * @param array $value
     */
    public function setParamsPUT(array $value)
    {
        $this->paramsPUT = $value;
    }
    
    protected $paramsPATCH;
    
    public function getParamsPATCH()
    {
        return $this->paramsPATCH;
    }

    /**
     * @param array $value
     */
    public function setParamsPATCH(array $value)
    {
        $this->paramsPATCH = $value;
    }
}