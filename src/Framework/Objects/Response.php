<?php
/**
 *
 * Description of Response
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 18/05/16 22:51
 *
*/

namespace Framework\Objects;


/**
 * Class Response
 * @package Framework\Objects
 */
class Response
{
    protected $statusCode;

    /**
     * @return int
     */
    public function getStatusCode() : int
    {
        return $this->statusCode;
    }

    /**
     * @param int $value
     */
    public function setStatusCode(int $value)
    {
        $this->statusCode = $value;
    }

    protected $status;

    /**
     * @return string
     */
    public function getStatus() : string
    {
        return $this->status;
    }

    /**
     * @param string $value
     */
    public function setStatus(string $value)
    {
        $this->status = $value;
    }

    protected $data;

    /**
     * @return string JSON
     */
    public function getData() : string
    {
        return $this->data;
    }

    /**
     * @param string $value
     */
    public function setData(string $value)
    {
        $this->data = $value;
    }
}