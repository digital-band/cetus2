<?php
/**
 *
 * Description of Launcher
 *
 * Initializes Framework instance for application
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/05/16 16:56
 *
*/
namespace Framework\Core;

use \Framework\Lib;

use \Framework\Objects;

/**
 * Class Launcher
 * @package Framework\Core
 */
class Launcher
{
    /**
     * Launcher constructor.
     */
    public function __construct()
    {
        require_once __DIR__ . '/../../Config/init.php';
    }

    public function run()
    {
        // TODO: add ModulePool to workspace
        $workspace = new Workspace();
        $router = new Router($workspace);

        $this->setWorkspace($workspace);

        // get URI part which refers to module
        $moduleURI = $router->getModuleURI();
        $moduleAlias = $router->getModuleAlias();

        $actionAlias = $router->getActionAlias();
        $resource = $router->getResource();
        $request = $router->getRequest();

        Lib\Logger::debug('Module URI:' . print_r($moduleURI, true));
        //Lib\Logger::debug('Resource: ' . print_r($resource, true));

        $this->handleRequest($router);

        echo 'AppName: ' . $this->getWorkspace()->getAppName() . '<br>';
        echo 'Module URI: ' . $moduleURI  . '<br>';
        echo 'Module Alias: ' . $moduleAlias  . '<br>';
        echo 'Action: ' . $actionAlias . '<br>';
        echo 'Resource Path: ' . $resource->getURI() . '<br>';
        echo 'Request: ' . print_r($request, true) . '<br>';
        echo 'Resource: ' . print_r($resource, true) . '<br>';
        echo 'Routing: ' . print_r($router->getRoute(), true) . '<br>';

    }

    // TODO: rework
    /**
     * If there is a custom module with the same alias as Lib module
     * and it does not extend Lib module then
     * custom module overrides it.
     *
     * @param Router $router
     */
    protected function handleRequest(Router $router)
    {
        $moduleURI = $router->getModuleURI();
        $modulesConfig = $this->getWorkspace()->getModuleConfigByPublicURI($moduleURI);
        $appModuleList = $this->getWorkspace()->getAppModuleListByPublicURI();

        // if need to load custom module
        //$isAppModuleRequested = is_array($appModuleList) && array_key_exists($moduleURI, $appModuleList);
        $isAppModuleRequested = (bool) $modulesConfig['isCustom'];

        Lib\Logger::debug(__METHOD__ . ' '
            . (($modulesConfig['isCustom']
                ? 'App'
                : 'Lib') . ' Module Request: ') . $moduleURI);

        Lib\Logger::debug(__METHOD__ . ' ' . 'ModulesConfig: ' . print_r($modulesConfig, true));

        // TODO: move module creating to own method. Might be used in modules.

        // read modules configs
        // find the right module
        // create object

        $modulesDirNamespace = '';
        $rootNamespace = '';
        $rootPath = ROOT;

        if ($isAppModuleRequested === true) {
            $rootNamespace = APP_ROOT_DIR_NAME;
            $config = $this->getWorkspace()->getAppConfig();
            $modulesDirNamespace = $config['appModulesDir'];

            $modulesDirPath = $config['appModulesDir'];
            $rootPath = APP_PATH;
        } else {
            $config = $this->getWorkspace()->getFrameworkConfig();
            $modulesDirNamespace = $config['libModulesDir'];

            $modulesDirPath = $config['libModulesDir'];
        }

        $alias = $modulesConfig['alias'];
        $moduleDirName = $modulesConfig['directory'];

        $fullModulePath = $rootPath . $modulesDirPath . $moduleDirName;

        Lib\Logger::debug(__METHOD__ . ' $rootNamespace' . ': ' . print_r($rootNamespace, true));
        Lib\Logger::debug(__METHOD__ . ' $modulesDirNamespace' . ': ' . print_r($modulesDirNamespace, true));

        Lib\Logger::debug(__METHOD__ . ' $fullModulePath' . ': ' . print_r($fullModulePath, true));

        Lib\Utils::pathToNamespace($fullModulePath, ROOT);
        Lib\Utils::pathToNamespace($fullModulePath . '/' . "{$alias}Module", ROOT);
        Lib\Utils::pathToNamespace($fullModulePath . "/{$alias}Module.php", ROOT);

        /*$module = $this->loadModule($alias);

        Lib\Logger::debug(__METHOD__ . ' $module' . ': ' . print_r($module, true));*/



    }

    /**
     * @param string $alias
     * @return Module
     */
    protected function loadModule(string $alias) : Module
    {

    }

    /**
     * @param string $moduleName
     *
     * DEL
     */
    protected function getModel(string $moduleName)
    {
        $appConfig = $this->getWorkspace()->getAppConfig();
        $commonConfig = $this->getWorkspace()->getFrameworkConfig();
        $modulesConfig = $this->getWorkspace()->getModulesConfig();

        Lib\Logger::debug(__METHOD__ . ' ' . 'App Config: ' . print_r($appConfig, true));
        Lib\Logger::debug(__METHOD__ . ' ' . 'Common Config: ' . print_r($commonConfig, true));
        Lib\Logger::debug(__METHOD__ . ' ' . 'Modules Config: ' . print_r($modulesConfig, true));
    }

    protected $workspace;

    /**
     * @return Workspace|null
     */
    public function getWorkspace()
    {
        return $this->workspace;
    }

    /**
     * @param Workspace $value
     */
    public function setWorkspace(Workspace $value)
    {
        $this->workspace = $value;
    }
}