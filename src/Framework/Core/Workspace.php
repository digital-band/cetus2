<?php
/**
 *
 * Description of Workspace
 *
 * Represents app state
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/05/16 16:39
 *
*/

namespace Framework\Core;

use Framework\Lib;

use Framework\Exceptions;

/**
 * Class Workspace
 * @package Framework\Core
 */
class Workspace
{
    public function __construct()
    {
        $fwConfig = $this->loadFrameworkConfig();
        $this->setFrameworkConfig($fwConfig);

        $appConfig = $this->loadAppConfig();
        $this->setAppConfig($appConfig);

        $fwModulesConfig = $this->loadFrameworkModulesConfig();
        $this->setFrameworkModulesConfig($fwModulesConfig);

        $appModulesConfig = $this->loadAppModulesConfig();
        $this->setAppModulesConfig($appModulesConfig);

        $modulesConfig = $this->loadModulesConfig();
        $this->setModulesConfig($modulesConfig);

        $mergedConfig = Lib\Utils::mergeArrays($fwConfig, $appConfig);

        //Lib\Logger::debug(__METHOD__ . ' ' . ': ' . print_r('------------{', true));
        //Lib\Logger::debug(__METHOD__ . ' ' . 'FrameworkModules: ' . print_r($fwModulesConfig, true));
        //Lib\Logger::debug(__METHOD__ . ' ' . 'AppModules: ' . print_r($appModulesConfig, true));
        //Lib\Logger::debug(__METHOD__ . ' ' . ': ' . print_r($mergedConfig, true));
        //Lib\Logger::debug(__METHOD__ . ' ' . ': ' . print_r('------------}', true));

        $modulesPublicURIList = $this->getModuleListByPublicURI();
        //Lib\Logger::debug(__METHOD__ . ' ' . '$modulesPublicURIList: ' . print_r($modulesPublicURIList, true));
        $this->setModulePublicURIList(array_keys($modulesPublicURIList));

        //Lib\Logger::debug(__METHOD__ . ' ' . '$mergedConfig: ' . print_r($mergedConfig, true));

        if (is_array($mergedConfig) && array_key_exists('defaultModuleAlias', $mergedConfig) ) {
            $defaultModuleConfig = $this->getModuleConfigByAlias($mergedConfig['defaultModuleAlias']);
            $this->setDefaultModuleURI($defaultModuleConfig['publicURI']);

        }

        $this->setAllowedActions($fwConfig['allowedActions']);
        $this->setAppName($appConfig['appName']);
    }


    /**
     * @return array|mixed
     * @throws Exceptions\WrongParamsException
     */
    protected function loadFrameworkConfig()
    {
        $result = [];
        $configFile = FRAMEWORK_CONFIG . '/config.json';

        $requiredFields = [
            'libModulesDir',
            'allowedActions'
        ];

        try {
            $result = $this->loadConfig($configFile);
        } catch (\InvalidArgumentException $e) {
            Lib\Logger::error(__METHOD__ . ': ' . $e->getMessage());
        }

        $errorEmptyFields = [];

        foreach ($requiredFields as $field) {
            if (array_key_exists($field, $result) === false) {
                $errorEmptyFields[] = $field;
            }
        }

        if (count($errorEmptyFields) !== 0) {
            $field = array_shift($errorEmptyFields);

            $errorMsg = "Bad framework config: '$field' not found";

            throw new Exceptions\WrongParamsException($errorMsg, 500);
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exceptions\WrongParamsException
     */
    protected function loadAppConfig() : array
    {
        $result = [];
        $configFile = APP_CONFIG_DIR . '/config.json';

        $requiredFields = [
            'appName',
            'appModulesDir'
        ];

        try {
            $result = $this->loadConfig($configFile);
        } catch (\InvalidArgumentException $e) {
            Lib\Logger::error(__METHOD__ . ': ' . $e->getMessage());
        }

        $errorEmptyFields = [];

        foreach ($requiredFields as $field) {
            if (array_key_exists($field, $result) === false) {
                $errorEmptyFields[] = $field;
            }
        }

        if (count($errorEmptyFields) !== 0) {
            $field = array_shift($errorEmptyFields);

            $errorMsg = "Bad application config: '$field' not found";

            throw new Exceptions\WrongParamsException($errorMsg, 500);
        }


        return $result;
    }

    /**
     * @return array
     * @throws \InvalidArgumentException
     * @throws Exceptions\WrongParamsException
     */
    protected function loadFrameworkModulesConfig() : array
    {
        $result = [];
        $commonConfig = $this->getFrameworkConfig();

        $commonModulesConfigFile = FRAMEWORK_CONFIG . '/../' . $commonConfig['libModulesDir'] . '/config.json';

        $commonModulesConfig = $this->loadConfig($commonModulesConfigFile);

        if ( array_key_exists('modules', $commonModulesConfig) === true ) {

            foreach ($commonModulesConfig['modules'] as $key => $value) {
                // add enabled modules only
                if ( $this->validateModuleConfig($value) === true && (bool) $value['isEnabled'] === true ) {
                    $value['isCustom'] = false;
                    $result[ucfirst($key)] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * @param array $config
     * @return bool
     * @throws Exceptions\WrongParamsException
     */
    private function validateModuleConfig(array $config)
    {
        $result = true;

        $requiredFields = [
            'publicURI',
            'adminURI',
            'directory',
            'isEnabled'
        ];
        $errorEmptyFields = [];

        foreach ($requiredFields as $field) {
            if (array_key_exists($field, $config) === false) {
                $errorEmptyFields[] = $field;
            }
        }

        if (count($errorEmptyFields) !== 0) {
            $field = array_shift($errorEmptyFields);

            $errorMsg = "Bad framework config: '$field' not found";

            throw new Exceptions\WrongParamsException($errorMsg, 500);
        }

        return $result;
    }

    /**
     * @return array
     * @throws \InvalidArgumentException
     * @throws Exceptions\WrongParamsException
     */
    protected function loadAppModulesConfig() : array
    {
        $result = [];
        $appConfig = $this->getAppConfig();

        $appModulesConfigFile = APP_CONFIG_DIR . '/../' . $appConfig['appModulesDir'] . '/config.json';

        $appModulesConfig = $this->loadConfig($appModulesConfigFile);

        if ( array_key_exists('modules', $appModulesConfig) === true ) {
            foreach ($appModulesConfig['modules'] as $key => $value) {

                // add enabled modules only
                if ( $this->validateModuleConfig($value) === true && (bool) $value['isEnabled'] === true ) {
                    $value['isCustom'] = true;
                    $result[ucfirst($key)] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * Merges Framework and App modules' configs
     *
     * @return array
     */
    protected function loadModulesConfig()
    {
        $result = [];

        $fwModulesConfig = $this->getFrameworkModulesConfig();
        $appModulesConfig = $this->getAppModulesConfig();

        if ( (is_array($fwModulesConfig) && is_array($appModulesConfig)) === true){
            $result = Lib\Utils::mergeArrays($fwModulesConfig, $appModulesConfig);
        }


        return $result;
    }

    /**
     * @param string $configFile
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function loadConfig(string $configFile)
    {
        if (file_exists($configFile) === true) {
            $configRaw = file_get_contents($configFile);
            $jsonParsed = json_decode($configRaw, true);

            if (json_last_error_msg() === 'No error') {
                return $jsonParsed;
            }

            $jsonParsedError = 'Invalid config file (' . $configFile . ')';

            Lib\Logger::error(__METHOD__ . ': ' . $jsonParsedError);

            throw new \InvalidArgumentException($jsonParsedError);
        }

        $configFileError = 'Config file not found in ' . $configFile;
        Lib\Logger::error(__METHOD__ . ': ' . $configFileError);

        throw new \InvalidArgumentException($configFileError);
    }

    /**
     * @param string $alias
     * @return array
     */
    public function getModuleConfigByAlias(string $alias)
    {
        $result = [];

        $alias = ucfirst($alias);

        $list = $this->getModuleListByAlias();

        if ( (is_array($list) && array_key_exists($alias, $list)) === true ) {
            $result = $list[$alias];
        }

        return $result;
    }

    /**
     * @param string $publicURI
     *
     * @return array
     */
    public function getModuleConfigByPublicURI(string $publicURI)
    {
        $result = [];

        $list = $this->getModuleListByPublicURI();

        if ( (is_array($list) && array_key_exists($publicURI, $list)) === true ) {
            $result = $list[$publicURI];
        }

        return $result;
    }

    /**
     * @param string $adminURI
     *
     * @return array
     */
    public function getModuleConfigByAdminURI(string $adminURI)
    {
        $result = [];

        $list = $this->getModuleListByAdminURI();

        if ( (is_array($list) && array_key_exists($adminURI, $list)) === true ) {
            $result = $list[$adminURI];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getAppModuleListByPublicURI() : array
    {
        $appModules = $this->getAppModulesConfig();
        $appModulesConverted = Lib\Utils::switchArrayKey($appModules, 'publicURI', 'alias');

        return $appModulesConverted;
    }

    /**
     * @return array
     */
    protected function getFrameworkModuleListByPublicURI() : array
    {
        $fwModules = $this->getFrameworkModulesConfig();
        $fwModulesConverted = Lib\Utils::switchArrayKey($fwModules, 'publicURI', 'alias');

        return $fwModulesConverted;
    }

    /**
     * Return array where keys are publicURI
     *
     * @return array
     */
    protected function getModuleListByPublicURI() : array
    {
        $fwModulesConverted = $this->getFrameworkModuleListByPublicURI();
        $appModulesConverted = $this->getAppModuleListByPublicURI();

        $result = Lib\Utils::mergeArrays($fwModulesConverted, $appModulesConverted);

        return $result;
    }

    /**
     * Return array where keys are AdminURI
     *
     * @return array
     */
    protected function getModuleListByAdminURI() : array
    {
        $fwModules = $this->getFrameworkModulesConfig();
        $appModules = $this->getAppModulesConfig();

        $fwModulesConverted = Lib\Utils::switchArrayKey($fwModules, 'adminURI', 'alias');
        $appModulesConverted = Lib\Utils::switchArrayKey($appModules, 'adminURI', 'alias');

        $result = Lib\Utils::mergeArrays($fwModulesConverted, $appModulesConverted);

        return $result;
    }

    /**
     * @return array
     */
    protected function getModuleListByAlias() : array
    {
        $fwModules = $this->getFrameworkModulesConfig();
        $appModules = $this->getAppModulesConfig();

        $fwModulesConverted = Lib\Utils::switchArrayKey($fwModules, 'alias');
        $appModulesConverted = Lib\Utils::switchArrayKey($appModules, 'alias');

        $result = Lib\Utils::mergeArrays($fwModulesConverted, $appModulesConverted);

        return $result;
    }

    protected $appConfig = [];

    /**
     * @return array
     */
    public function getAppConfig() : array
    {
        return $this->appConfig;
    }

    /**
     * @param array $value
     */
    public function setAppConfig(array $value)
    {
        $this->appConfig = $value;
    }

    protected $frameworkConfig = [];

    /**
     * @return array
     */
    public function getFrameworkConfig() : array
    {
        return $this->frameworkConfig;
    }

    /**
     * @param array $value
     */
    public function setFrameworkConfig(array $value)
    {
        $this->frameworkConfig = $value;
    }

    protected $modulesConfig = [];

    public function getModulesConfig() : array
    {
        return $this->modulesConfig;
    }

    public function setModulesConfig(array $value)
    {
        $this->modulesConfig = $value;
    }

    protected $allowedActions = [];

    /**
     * @return array
     */
    public function getAllowedActions() : array
    {
       return $this->allowedActions;
    }

    /**
     * @param array $value
     */
    public function setAllowedActions(array $value)
    {
       $this->allowedActions = $value;
    }

    protected $modulePublicURIList= [];

    /**
     * @return array
     */
    public function getModulePublicURIList() : array
    {
        return $this->modulePublicURIList;
    }

    public function setModulePublicURIList(array $value)
    {
        $this->modulePublicURIList = $value;
    }

    protected $appName;

    /**
     * @return string
     */
    public function getAppName() : string
    {
        return $this->appName;
    }

    /**
     * @param string $value
     */
    public function setAppName(string $value)
    {
        $this->appName = $value;
    }

    // MVC state
    protected $model;

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($value)
    {
        $this->model = $value;
    }

    protected $controller;

    public function getController()
    {
        return $this->controller;
    }

    public function setController($value)
    {
        $this->controller = $value;
    }

    protected $view;

    public function getView()
    {
        return $this->view;
    }

    public function setView($value)
    {
        $this->view = $value;
    }

    protected $defaultModuleURI = '';

    /**
     * @return string
     */
    public function getDefaultModuleURI() : string
    {
        return $this->defaultModuleURI;
    }

    /**
     * @param string $value
     */
    public function setDefaultModuleURI(string $value)
    {
        $this->defaultModuleURI = $value;
    }

    protected $DefaultModuleAlias;

    /**
     * @return string
     */
    public function getDefaultModuleAlias() : string
    {
        return $this->DefaultModuleAlias;
    }

    /**
     * @param string $value
     */
    public function setDefaultModuleAlias(string $value)
    {
        $this->DefaultModuleAlias = $value;
    }

    protected $frameworkModulesConfig = [];

    public function getFrameworkModulesConfig()
    {
        return $this->frameworkModulesConfig;
    }

    public function setFrameworkModulesConfig($value)
    {
        $this->frameworkModulesConfig = $value;
    }

    protected $appModulesConfig = [];

    public function getAppModulesConfig()
    {
        return $this->appModulesConfig;
    }

    public function setAppModulesConfig($value)
    {
        $this->appModulesConfig = $value;
    }
}