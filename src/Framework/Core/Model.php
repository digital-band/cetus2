<?php
/**
 *
 * Description of Model
 *
 * Basic model class
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/05/16 16:40
 *
*/

namespace Framework\Core;


/**
 * Class Model
 * @package Framework\Core
 */
abstract class Model
{

    final public function __construct($module)
    {
        // TODO: do not override constructor, run init method of child if exists instead
        error_log(__METHOD__);
        error_log(print_r('Basic Model final constructor loaded', true));

        $this->setModule($module);
    }

    protected $module;

    public function getModule() : Module
    {
        return $this->module;
    }

    public function setModule(Module $value)
    {
        $this->module = $value;
    }
}