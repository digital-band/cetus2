<?php
/**
 *
 * Description of Router
 *
 * Implements basic URI methods
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/05/16 16:37
 *
*/

namespace Framework\Core;

use Framework\Lib;
use Framework\Objects;
use Framework\Constants;
use Framework\Exceptions;

/**
 * Class Router
 * @package Framework\Core
 */
class Router
{
    /**
     *
     * Can handle current request or passed URI
     * @param Workspace $workspace
     *
     * @throws \Exception
     *
    */
    public function __construct(Workspace $workspace)
    {
        $this->setWorkspace($workspace);

        $request = $this->parseRequest();
        $this->setRequest($request);

        $uri = $request->getURI();
        $uriParts = $this->splitURI($uri);

        $moduleURI = $this->_getModuleURI($uriParts);
        $this->setModuleURI($moduleURI);

        $moduleAlias = $this->_getModuleAlias($moduleURI);
        $this->setModuleAlias($moduleAlias);

        $actionAlias = $this->_getActionAlias($uriParts);
        $this->setActionAlias($actionAlias);

        $resource = $this->_getResource($moduleURI, $actionAlias, $uriParts);
        $this->setResource($resource);

        $route = $this->getRoutingData($moduleURI, $resource);
        $this->setRoute($route);
    }

    /**
     * @param string $moduleURI
     * @param Objects\Resource $resource
     * @return Objects\Route
     */
    protected function getRoutingData(string $moduleURI, Objects\Resource $resource) : Objects\Route
    {
        $route = new Objects\Route();

        // try to read custom routes
        $routingData = $this->getCustomRoutingData($resource->getURI(), $moduleURI);

        if (array_key_exists('uriMask', $routingData) === true) {
            $args = $this->extractArguments($resource->getURI(), $routingData['uriMask']);

            $controllerName = $routingData['controller'];
            $actionMethod = $routingData['actionMethod'];

        } else {
            // auto routing scheme: <http_method><CollectionAlias><ActionAlias>Action
            $moduleConfig = $this->getWorkspace()->getModuleConfigByPublicURI($moduleURI);
            $moduleAlias = $moduleConfig['alias'];

            $controllerName = $moduleAlias;

            $collectionList = $resource->getCollections();
            $args = $resource->getCollectionMembers();

            $collection = end($collectionList);

            $actionMethod = strtolower($this->getRequest()->getMethod())
                . ucfirst($collection)
                . ucfirst($this->getActionAlias())
                . 'Action';
        }

        $route->setControllerName($controllerName);
        $route->setActionMethod($actionMethod);
        $route->setArguments($args);


        return $route;
    }

    /**
     * Extracts items' ids from URI
     *
     * @param string $resourceUri
     * @param string $uriMask
     * @return array
     */
    protected function extractArguments(string $resourceUri, string $uriMask)
    {
        $result = [];

        $uriMaskParts = $this->splitURI($uriMask);

        $resourcePathArray = $this->splitURI($resourceUri);
        $indexMax = count($resourcePathArray);

        for ($index = 0; $index < $indexMax; $index++) {
            if ($resourcePathArray[$index] !== $uriMaskParts[$index]) {
                $variable = preg_replace('/[\{\}]/', '', $uriMaskParts[$index]);

                $variableInfo = explode(':', $variable);

                $variableName = $variableInfo[0];
                $variableValue = $resourcePathArray[$index];

                $validationPattern = $variableInfo[1] ?? null;

                if ($validationPattern === null
                    || ($validationPattern !== null && preg_match('/^' . $validationPattern . '$/', $variableValue) === 1)
                ) {
                    $result[$variableName] = $variableValue;
                }
            }
        }


        return $result;
    }

    /**
     * @param string $resourceUri
     * @return array
     */
    protected function extractCollectionMembers(string $resourceUri) : array
    {
        $result = [];
        $argsArray = [];

        preg_match_all('/(\b|\/^\/)[a-z]+\/[A-z\-\d]+/', $resourceUri, $argsArray);

        if (count($argsArray) !== 0) {
            foreach ($argsArray[0] as $variable) {
                $variableInfo = explode('/', $variable);

                $variableName = $variableInfo[0];
                $variableValue = $variableInfo[1] ?? null;

                if ($variableValue !== null) {
                    $result[$variableName] = $variableValue;
                }
            }
        }

        return $result;
    }

    /**
     *
     * Extracts collections from resource URI
     *
     * @param string $resourceUri
     * @return array
     */
    protected function extractCollections(string $resourceUri) : array
    {
        $result = [];

        preg_match_all('/(\/)[a-z]+((\/([A-z\-\d]+))|[a-z]\/)/', '/' . $resourceUri . '/', $collectionArray);

        if (count($collectionArray) > 0) {
            foreach ($collectionArray[0] as $part) {
                $part = preg_filter('/^(\/)|(\/)$/', '', $part);

                $sections = explode('/', $part);

                $isValid = preg_match('/[a-z]+/', $sections[0]);

                if ($isValid === 1) {
                    $result[] = $sections[0];
                }
            }
        }


        return $result;
    }

    /**
     *
     * Returns URI parts as array
     *
     * @param string $uri
     *
     * @return array
     *
    */
    public function splitURI(string $uri = null) : array
    {
        $result = [];

        $parsedURI = $this->parseURI($uri);
        $uri = $parsedURI['URI'];

        // explode and hide empty parts
        $uriParts = array_filter(explode('/', $uri));

        foreach ($uriParts as $k => $v) {
            // clean from GET params
            if (0 === strpos($v, '?')) {
                unset($uriParts[$k]);
            }
        }

        foreach ($uriParts as $k => $v) {
            $result[] = $v;
        }

        return $result;
    }

    /**
     *
     * Explodes get query string to array
     *
     * @param string $query
     * @return array
     */
    protected function prepareParamsGET(string $query) : array
    {
        $result = [];

        $paramArray = explode('&', $query);

        foreach ($paramArray as $item) {
            $param = explode('=', $item);

            if (count($param) !== 0) {
                $value = $param[1] ?? null;

                $result[$param[0]] = $value;
            }
        }

        Lib\Logger::debug(__METHOD__ . ' GET' . ': ' . print_r($_GET, true));

        return $result;
    }

    /**
     * Returns URI and GET params separately as array
     *
     * @param string $uri
     * @return array
     */
    protected function parseURI(string $uri) : array
    {
        $result = [
            'URI' => '',
            'Params' => []
        ];

        $uriParts = explode('?', $uri);

        $uriSections = explode('/', $uriParts[0]);
        if ( array_key_exists(0, $uriSections) && $uriSections[0] === ADMIN_URI ){
            // admin request

        }

        $result['URI'] = $uriParts[0];

        // collect GET params
        if (!empty($uriParts[1])) {
            $paramString = $uriParts[1];

            $result['Params'] = $this->prepareParamsGET($paramString);
        }

        Lib\Logger::debug(__METHOD__ . ' ' . 'Parsed URI: ' . print_r($result, true));

        return $result;
    }

    /**
     * Request object contains info about current HTTP request
     *
     * @return Objects\Request
     * @throws \InvalidArgumentException
     */
    protected function parseRequest() : Objects\Request
    {
        $request = new Objects\Request();

        $uri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);
        $method = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_DEFAULT);
        $contentType = filter_input(INPUT_SERVER, 'CONTENT_TYPE', FILTER_DEFAULT) ?? '';

        $paramsRequest = $_REQUEST;
        $parsedURI = $this->parseURI($uri);

        $request->setURI($parsedURI['URI']);
        $request->setMethod($method);
        $request->setParams($paramsRequest);

        if ($method === Constants\Request::METHOD_PUT) {
            $request->setParamsPUT($this->parseRequestParams(Constants\Request::METHOD_PUT, $contentType));
        }
        if ($method === Constants\Request::METHOD_PATCH) {
            $request->setParamsPATCH($this->parseRequestParams(Constants\Request::METHOD_PATCH, $contentType));
        }
        if ($method === Constants\Request::METHOD_POST) {
            $request->setParamsPOST($this->parseRequestParams(Constants\Request::METHOD_POST, $contentType));
        }

        $request->setParamsGET($this->parseRequestParams(Constants\Request::METHOD_GET, $contentType));


        return $request;
    }

    /**
     * @param string $method
     * @param string $contentType
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function parseRequestParams(string $method, string $contentType) : array
    {
        $result = [];

        if ($method === Constants\Request::METHOD_PUT || $method === Constants\Request::METHOD_PATCH) {
            $raw = file_get_contents('php://input');

            if ($raw === '') {
                return $result;
            }

            if ($contentType === 'application/json') {
                $jsonParsed = json_decode($raw);

                if (json_last_error_msg() === 'No error') {
                    return $jsonParsed;
                }
            }

            $errorText = 'Bad request: JSON request must have "application/json" Content-Type header ';
            Lib\Logger::error(__METHOD__ . ': ' . $errorText);

            throw new \InvalidArgumentException($errorText, -1);
        }

        if ($method === Constants\Request::METHOD_GET) {
            $raw = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_URL);

            return $this->prepareParamsGET($raw);
        }

        if ($method === Constants\Request::METHOD_POST) {

            return $_POST;
        }

        $errorText = 'Bad request: unsupported HTTP method';
        Lib\Logger::error(__METHOD__ . ': ' . $errorText);

        throw new \InvalidArgumentException($errorText, -1);
    }

    /**
     * Returns Module URI
     *
     * @param array $uriParts
     *
     * @return String
     * @throws \Exception
     *
    */
    private function _getModuleURI(array $uriParts = null) : string
    {
        if ($uriParts === null) {
            $uriParts = $this->getUriParts();
        }

        $part = '';

        if (count($uriParts) !== 0) {
            $part = strtolower($uriParts[0]);
        }

        $moduleUriList = $this->getWorkspace()->getModulePublicURIList();

        if ( in_array($part, $moduleUriList, true) === true ) {
            $result = $part;
        } else {
            $result = $this->getWorkspace()->getDefaultModuleURI();
            Lib\Logger::debug(__METHOD__ . ' Default Module' . ': ' . print_r($result, true));
        }

        return $result;
    }

    /**
     * Finds module's Alias by URI from Config
     *
     * @param $moduleURI
     */
    private function _getModuleAlias(string $moduleURI)
    {
        $modulesConfig = $this->getWorkspace()->getModuleConfigByPublicURI($moduleURI);

        return $modulesConfig['alias'];
    }

    /**
     * @param array|null $uriParts
     * @return string
     */
    private function _getActionAlias(array $uriParts = null) : string
    {
        // default action
        $result = 'get';

        if ($uriParts === null) {
            $uriParts = $this->getUriParts();
        }

        $part = strtolower(end($uriParts));

        if (in_array($part, $this->getWorkspace()->getAllowedActions(), true) === true) {
            $result = (String) $part;
        }

        return $result;
    }

    /**
     * @param string $moduleAlias
     * @param string $actionAlias
     * @param array|null $uriParts
     * @return Objects\Resource
     */
    private function _getResource(string $moduleAlias, string $actionAlias, array $uriParts = null) : Objects\Resource
    {
        $resourceUriParts = [];

        if ($uriParts === null) {
            $uriParts = $this->getUriParts();
        }

        foreach ($uriParts as $item){
            if ($item !== $moduleAlias && $item !== $actionAlias) {

                $resourceUriParts[] = $item;
            }
        }

        $resourcePath = implode('/', $resourceUriParts);
        $collectionList = $this->extractCollections($resourcePath);
        $memberList = $args = $this->extractCollectionMembers($resourcePath);

        $resource = new Objects\Resource();
        $resource->setURI($resourcePath);
        $resource->setUriParts($resourceUriParts);
        $resource->setCollections($collectionList);
        $resource->setCollectionMembers($memberList);


        return $resource;
    }

    /**
     * Loads routes from route file
     *
     * @param string $resourcePath
     * @param string $moduleURI
     * @return array
     * @throws Exceptions\WrongParamsException
     * @throws \InvalidArgumentException
     */
    protected function getCustomRoutingData(string $resourcePath, string $moduleURI) : array
    {
        $moduleConfig = $this->getWorkspace()->getModuleConfigByPublicURI($moduleURI);

        if ( (bool) $moduleConfig['isCustom'] === true ) {
            $appConfig = $this->getWorkspace()->getAppConfig();
            $directoryPath = $appConfig['appModulesDir'] . '/' . ucfirst($moduleConfig['alias']) . '/Controllers';

            $filePath = APP_PATH . '/' . $directoryPath . '/routes.json';
        } else {
            $fwConfig = $this->getWorkspace()->getFrameworkConfig();
            $directoryPath = ROOT . '/' . $fwConfig['libModulesDir'] . '/' . ucfirst($moduleConfig['alias']) . '/Controllers';

            $filePath = $directoryPath . '/routes.json';
        }

        //$filePath = APP_PATH . '/' . $directoryPath . '/routes.json';

        $routes = $this->getWorkspace()->loadConfig($filePath);

        $result = [];

        foreach ( (array) $routes as $route) {

            if ($this->validateRoute($route) === true &&
                $this->matchURI($resourcePath, $route['uriMask']) === true &&
                $this->getRequest()->getMethod() === $route['method'] &&
                $this->getActionAlias() === strtolower($route['action'])
            ) {

                return $route;
            }
        }

        return $result;
    }

    /**
     * @param array $route
     * @return bool
     */
    protected function validateRoute(array $route)
    {
        if (array_key_exists('method', $route) === false) {
            return false;
        }
        if (array_key_exists('action', $route) === false) {
            return false;
        }
        if (array_key_exists('uriMask', $route) === false) {
            return false;
        }

        return true;
    }

    /**
     * @param string $uri
     * @param string $mask
     * @return bool
     */
    private function matchURI(string $uri, string $mask)
    {
        $result = true;

        $uriArray = $this->splitURI($uri);
        $maskArray = $this->splitURI($mask);

        $maxCount = count($uriArray);
        if ($maxCount !== count($maskArray)) {
            return false;
        }

        for ($index = 0; $index < $maxCount; $index++) {
            if (preg_match('/[\{\}]/', $maskArray[$index]) === 1) {
                // must match mask
                // TODO code duplication
                $variable = preg_replace('/[\{\}]/', '', $maskArray[$index]);
                $variableInfo = explode(':', $variable);

                $variableValue = $uriArray[$index];
                $validationPattern = $variableInfo[1] ?? null;

                $result = $validationPattern === null ||
                    preg_match('/^' . $validationPattern . '$/', $variableValue) === 1;

            } else {
                // must be equal
                $result = $maskArray[$index] === $uriArray[$index];
            }

            if ($result === false) {
                return $result;
            }
        }

        return $result;
    }

    // getters, setters

    protected $moduleAlias = '';

    /**
     * @return string
     */
    public function getModuleAlias() : string
    {
        return $this->moduleAlias;
    }

    /**
     * @param string $value
     */
    public function setModuleAlias(string $value)
    {
        $this->moduleAlias = $value;
    }

    protected $moduleURI;

    /**
     * @return string
     */
    public function getModuleURI() : string
    {
        return $this->moduleURI;
    }

    /**
     * @param string $value
     */
    public function setModuleURI(string $value)
    {
        $this->moduleURI = $value;
    }

    protected $actionAlias;

    /**
     * @return string
     */
    public function getActionAlias() : string
    {
        return $this->actionAlias;
    }

    /**
     * @param string $value
     */
    public function setActionAlias(string $value)
    {
        $this->actionAlias = $value;
    }

    protected $resource;

    /**
     * @return Objects\Resource
     */
    public function getResource() : Objects\Resource
    {
        return $this->resource;
    }

    /**
     * @param Objects\Resource $value
     */
    public function setResource(Objects\Resource $value)
    {
        $this->resource = $value;
    }

    protected $request;

    /**
     * @return Objects\Request
     */
    public function getRequest() : Objects\Request
    {
        return $this->request;
    }

    /**
     * @param Objects\Request $value
     */
    public function setRequest(Objects\Request $value)
    {
        $this->request = $value;
    }

    protected $uriParts;

    /**
     * @return array
     */
    public function getUriParts() : array
    {
        return $this->uriParts;
    }

    /**
     * @param array $value
     */
    public function setUriParts(array $value)
    {
        $this->uriParts = $value;
    }

    protected $route;

    /**
     * @return Objects\Route
     */
    public function getRoute() : Objects\Route
    {
        return $this->route;
    }

    /**
     * @param Objects\Route $value
     */
    public function setRoute(Objects\Route $value)
    {
        $this->route = $value;
    }

    protected $workspace;

    /**
     * @return Workspace
     */
    public function getWorkspace()
    {
        return $this->workspace;
    }

    /**
     * @param Workspace $value
     */
    public function setWorkspace(Workspace $value)
    {
        $this->workspace = $value;
    }
}