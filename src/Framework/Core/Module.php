<?php
/**
 *
 * Description of Module
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 22/05/16 17:50
 *
*/

namespace Framework\Core;

use Framework\Lib;

use Framework\Exceptions;

abstract class Module
{
    public function __construct($moduleNamespace)
    {
        $this->setModuleNamespace($moduleNamespace);
        echo 'Basic Module loaded!', '<br>';
    }

    /**
     *
     */
    public function load()
    {

    }

    /**
     * Register model from the same Module
     *
     * @param string $modelAlias
     */
    protected function registerModel(string $modelAlias)
    {
        $modelClassName = ucfirst($modelAlias) . 'Model';
        $this->getModuleNamespace();
        $modelNamespace = $this->getModuleNamespace() . '\\Models\\' . $modelClassName;

        // Lazy model initialization. Object shouldn't be created instantly, so put it to init stack.
        $this->addModelToInitStack($modelAlias, $modelNamespace);
    }

    /**
     * @param string $modelAlias
     * @return Model
     * @throws Exceptions\WrongParamsException
     */
    public function getModel(string $modelAlias) : Model
    {
        // track model's usage
        $caller = $this->getCallingClass();
        $pool = $this->getModelPool();

        $model = $pool[$modelAlias] ?? null;

        // if model is not created yet
        if ($model === null) {
            $modelNamespace = $this->getModelFromInitStack($modelAlias);

            Lib\Logger::debug(__METHOD__ . ' ' . 'Model from stack: ' . print_r($modelNamespace, true));
            $model = new $modelNamespace($this);

            $this->addModelToPool($modelAlias, $model);
        }

        if ($model !== null && $caller !== '') {
            $callers = $this->getModelCallers();
            $callers[$modelAlias][] = $caller;
            $this->setModelCallers($callers);
        }

        if ($model === null) {
            Lib\Logger::error(__METHOD__ . ' ' . 'There is no such registered model: ' . print_r($modelAlias, true));

            throw new Exceptions\WrongParamsException('There is no such registered model: ' . $modelAlias, 404);
        }

        return $model;
    }

    /**
     * @param string $modelAlias
     * @param string $modelNamespace
     */
    private function addModelToInitStack(string $modelAlias, string $modelNamespace)
    {
        $stack = $this->getModelInitStack();

        if (array_key_exists($modelAlias, $stack) === false) {
            $stack[$modelAlias] = $modelNamespace;
            $this->setModelInitStack($stack);
        }
    }

    /**
     * @param string $modelAlias
     * @return string
     * @throws Exceptions\WrongParamsException
     */
    private function getModelFromInitStack(string $modelAlias) : string
    {
        $stack = $this->getModelInitStack();

        $modelInfo = $stack[$modelAlias] ?? null;

        if ($modelInfo === null) {
            Lib\Logger::error(__METHOD__ . ' ' . 'There is no such registered model: ' . print_r($modelAlias, true));

            throw new Exceptions\WrongParamsException('There is no such registered model: ' . $modelAlias, 404);
        }

        return $modelInfo;
    }

    /**
     * @param string $modelAlias
     * @param Model $model
     */
    private function addModelToPool(string $modelAlias, Model $model)
    {
        $pool = $this->getModelPool();

        if (array_key_exists($modelAlias, $pool) === false) {
            $pool[$modelAlias] = $model;
            $this->setModelPool($pool);
        }
    }

    /**
     * @param string $modelAlias
     * @param Model $model
     */
    private function addForeignModelToPool(string $modelAlias, Model $model)
    {
        $pool = $this->getForeignModelPool();

        if (array_key_exists($modelAlias, $pool) === false) {
            $pool[$modelAlias] = $model;
            $this->setForeignModelPool($pool);
        }
    }

    /**
     * Register model from another Module
     *
     * @param string $moduleName
     * @param string $modelAlias
     */
    protected function registerForeignModel(string $moduleName, string $modelAlias)
    {
        // get module path from config by alias
        $commonConfig = Workspace::getInstance()->getFrameworkConfig();

        $modulePath = $commonConfig['modulesDir']  . "$moduleName/{$moduleName}Module";
        $moduleNamespace = str_replace('/', '\\', $modulePath);

        $foreignModule = new $moduleNamespace;
        $foreignModel = $foreignModule->getModel($modelAlias);

        $this->addForeignModelToPool($modelAlias, $foreignModel);
    }

    /**
     * https://gist.github.com/hamstar/1122679
     * @return mixed
     */
    private function getCallingClass()
    {
        //get the trace
        $trace = debug_backtrace();

        // Get the class that is asking for who awoke it
        $class = $trace[1]['class'];

        // +1 to i cos we have to account for calling this function
        $count = count( $trace );

        for ( $i = 1; $i < $count; $i++ ) {
            if ( array_key_exists($i, $trace) ) // is it set?
                // is it a different class
                if ( $class !== $trace[$i]['class'] ) {

                    return $trace[$i]['class'];
                }
        }
    }

    private $modelPool = [];

    /**
     * @return array
     */
    private function getModelPool() : array
    {
        return $this->modelPool;
    }

    /**
     * @param array $value
     */
    private function setModelPool(array $value)
    {
        $this->modelPool = $value;
    }

    private $modelInitStack = [];

    /**
     * @return array
     */
    private function getModelInitStack() : array
    {
        return $this->modelInitStack;
    }

    /**
     * @param array $value
     */
    private function setModelInitStack(array $value)
    {
        $this->modelInitStack = $value;
    }

    private $controllerPool = [];

    private function getControllerPool() : array
    {
        return $this->controllerPool;
    }

    private function setControllerPool(array $value)
    {
        $this->controllerPool = $value;
    }

    protected $moduleNamespace;

    public function getModuleNamespace()
    {
        return $this->moduleNamespace;
    }

    public function setModuleNamespace($value)
    {
        $this->moduleNamespace = $value;
    }

    private $modelCallers = [];

    private function getModelCallers() : array
    {
        return $this->modelCallers;
    }

    private function setModelCallers(array $value)
    {
        $this->modelCallers = $value;
    }

    private $foreignModelPool = [];

    /**
     * @return array
     */
    private function getForeignModelPool() : array
    {
        return $this->foreignModelPool;
    }

    /**
     * @param array $value
     */
    private function setForeignModelPool(array $value)
    {
        $this->foreignModelPool = $value;
    }
}