<?php

/**
 *
 * Description of Controller
 *
 * Basic controller class
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/05/16 14:57
 *
*/

namespace Framework\Core;

/**
 * Class Controller
 * @package Framework\Core
 */
abstract class Controller
{
    abstract public function default();

    protected $model;
    
    public function getModel()
    {
        return $this->model;
    }
    
    public function setModel($value)
    {
        $this->model = $value;
    }
}
