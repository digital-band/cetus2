<?php

/**
 * Common config for the Framework
 */
// TODO move to app config
const LOG_PATH = __DIR__ . '/../../var/logs';

ini_set('display_errors', 1);
ini_set('log_errors', 1);
ini_set('error_log', LOG_PATH . 'error.log');

$appPath = dirname($_SERVER['DOCUMENT_ROOT']);
$appConfigDir = $appPath . '/Config';

$appRootArray = explode('/', $appPath);
$appRootDirName = count($appRootArray) > 1 ? $appRootArray[count($appRootArray) - 2] : '';

define('ROOT', dirname(__DIR__) );
define('FRAMEWORK_CONFIG', __DIR__);
define('APP_PATH', $appPath);
define('APP_ROOT_DIR_NAME', $appRootDirName);
define('APP_CONFIG_DIR', $appConfigDir);
// TODO move to app config
define('ADMIN_URI', 'admin');
define('ENV_TYPE', 'DEV');


date_default_timezone_set('Europe/Moscow');

require __DIR__ . '/../../vendor/autoload.php';


\Framework\Lib\Logger::debug(__METHOD__ . ' ' . 'App dir: ' . print_r($appRootDirName, true));
\Framework\Lib\Logger::debug('Config loaded');
